UNIT BaseGzLibWrapper;

{$mode objfpc}{$H+}

INTERFACE

USES
   Classes,
   SysUtils;



//string
FUNCTION GzBaseStr (CONST s   : String )  : String;
FUNCTION unGzBaseStr (CONST s   : String )  : String;

//file GzBase
PROCEDURE GzBaseFile (Vstup, vystup   : String );
PROCEDURE unGzBaseFile (Vstup, vystup   : String );

//File Base
PROCEDURE unBaseFile (Vstup, vystup   : String );
PROCEDURE BaseFile (Vstup, vystup   : String );

//base64
FUNCTION base64_encode (s   : String )  : String;
FUNCTION base64_decode (s   : String )  : String;

IMPLEMENTATION

CONST
   lib = 'BaseGzLib.dll';

//string
FUNCTION GzBaseStr (CONST s   : String )  : String; external lib;
FUNCTION unGzBaseStr (CONST s   : String )  : String; external lib;

//file GzBase
PROCEDURE GzBaseFile (Vstup, vystup   : String ); external lib;
PROCEDURE unGzBaseFile (Vstup, vystup   : String ); external lib;

//File Base
PROCEDURE unBaseFile (Vstup, vystup   : String ); external lib;
PROCEDURE BaseFile (Vstup, vystup   : String ); external lib;

//base64
FUNCTION base64_encode (s   : String )  : String; external lib;
FUNCTION base64_decode (s   : String )  : String; external lib;


END.
