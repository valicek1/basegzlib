library BaseGzLib;

{$mode objfpc}{$H+}

uses
  GzBaseLib;

exports
  GzBaseStr, unGzBaseStr,
  GzBaseFile, unGzBaseFile,
  BaseFile, unBaseFile,
  base64_encode, base64_decode;

{$R *.res}

begin
end.

