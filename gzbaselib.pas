UNIT GzBaseLib;

{$mode objfpc}{$H+}

INTERFACE

USES
   base64,
   Classes,
   SysUtils,
   zstream,
   process;

//string
FUNCTION GzBaseStr (CONST s   : String )  : String; export;
FUNCTION unGzBaseStr (CONST s   : String )  : String; export;

//file GzBase
PROCEDURE GzBaseFile (Vstup, vystup   : String ); export;
PROCEDURE unGzBaseFile (Vstup, vystup   : String ); export;

//File Base
PROCEDURE unBaseFile (Vstup, vystup   : String ); export;
PROCEDURE BaseFile (Vstup, vystup   : String ); export;

//base64
function base64_encode(s: string): string; export;
function base64_decode(s: string): string; export;

function FileToString(const FileName: TFileName): String;
Procedure StringToFile(Str, Soub: string);


IMPLEMENTATION

var
   BASE64exe, GZIPexe: String;
   Slozka: string;

Procedure WriteBASE64exe(path: string);
begin

   StringToFile(BASE64exe, path);
end;

Procedure WriteGZIPexe(path: string);
begin
   StringToFile(GZIPexe, path);
end;

FUNCTION ArrToStr (a   : ARRAY OF String )  : String;
VAR
   i  : Integer;
BEGIN
   Result := '';
   FOR i := 0 TO Length (a ) - 1 DO
   BEGIN
      Result := Result + a[i];
   END;
END;

function FileToString(const FileName: TFileName): String;
var
  FileStream : TFileStream;
begin
  FileStream:= TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
     if FileStream.Size>0 then
     begin
      SetLength(Result, FileStream.Size);
      FileStream.Read(Pointer(Result)^, FileStream.Size);
     end;
    finally
     FileStream.Free;
    end;
end;

Procedure StringToFile(Str, Soub: string);
var
  soubor: TextFile;
begin
  try
    AssignFile(soubor, soub);
    Rewrite(soubor);
    Write(soubor, str);
  finally
    CloseFile(soubor);
  end;
end;


FUNCTION GzBaseStr (CONST s   : String )  : String; export;
VAR
   OutStream, InpStream, GzStream, b64Stream  : TStream;
BEGIN
   OutStream := TStringStream.Create ('' );
   TRY
      b64Stream := TBase64EncodingStream.Create (OutStream );
      TRY
         GzStream := Tcompressionstream.Create (clmax, b64Stream );
         TRY
            InpStream := TStringStream.Create (s );
            TRY
               // Copy input stream
               GzStream.CopyFrom (InpStream, InpStream.Size );
            FINALLY
               InpStream.Free;
            END;
         FINALLY
            GzStream.Free;
         END;
      FINALLY
         b64Stream.Free;
      END;
      Result := TStringStream (OutStream ).DataString;
   FINALLY
      OutStream.Free;
   END;
END;


FUNCTION unGzBaseStr (CONST s   : String )  : String; export;
VAR
   OutStream, deCompressStream  : TStream;
   SL  : TStringList;

BEGIN
   IF (s = '' ) THEN
   BEGIN
      Result := '';
      abort;
   END;

   SL := TStringList.Create;
   OutStream := TStringStream.Create (DecodeStringBase64 (s ) );
   DecompressStream := TDecompressionStream.Create (OutStream );

   TRY
      Sl.Clear;
      SL.LoadFromStream (DecompressStream );
      Result := SL.Text;
   FINALLY
      DecompressStream.Free;
      OutStream.Free;
      SL.Free;
   END;

END;

function base64_encode(s: string): string; export;
begin
   result := '';
   try
     result := EncodeStringBase64(S);
   except
   end;
end;

function base64_decode(s: string): string; export;
begin
   result := '';
   try
     result := DecodeStringBase64(S);
   except
   end;
end;


PROCEDURE GzBaseFile (Vstup, vystup   : String ); export;
var
   Proc: TProcess;
begin
   WriteBASE64exe(Slozka + '\base64.exe');
   WriteGZIPexe(Slozka + '\gzip.exe');

   Proc := TProcess.Create(nil);
   Proc.Options := Proc.Options + [poWaitOnExit];
   Proc.ShowWindow := swoHIDE;
   Proc.{%H-}CommandLine := Format(Slozka + '\gzip.exe "%s"', [Vstup]);
   Proc.Execute;
   Proc.{%H-}CommandLine := Format(Slozka + '\base64.exe -e "%s.gz" "%s"', [Vstup, vystup]);
   Proc.Execute;
   DeleteFile(vstup + '.gz');
   Proc.Free;
   DeleteFile(Slozka + '\base64.exe');
   DeleteFile(Slozka + '\gzip.exe');
end;

PROCEDURE unGzBaseFile (Vstup, vystup   : String ); export;
var
   Proc: TProcess;
begin
   WriteBASE64exe(Slozka + '\base64.exe');
   WriteGZIPexe(Slozka + '\gzip.exe');

   Proc := TProcess.Create(nil);
   Proc.Options := Proc.Options + [poWaitOnExit];
   Proc.ShowWindow := swoHIDE;
   Proc.{%H-}CommandLine := Format(Slozka + '\base64.exe -d "%s" "%s.gz"', [Vstup, vystup]);
   Proc.Execute;
   Proc.{%H-}CommandLine := Format(Slozka + '\gzip.exe -d "%s"', [Vystup]);
   Proc.Execute;
   DeleteFile(vstup);
   Proc.Free;
   DeleteFile(Slozka + '\base64.exe');
   DeleteFile(Slozka + '\gzip.exe');
end;


PROCEDURE BaseFile (Vstup, vystup   : String ); export;
var
   Proc: TProcess;
begin
   if not FileExists(vstup) then exit;
   WriteBASE64exe(Slozka + '\base64.exe');
   Proc := TProcess.Create(nil);
   Proc.Options := Proc.Options + [poWaitOnExit];
   Proc.ShowWindow := swoHIDE;
   Proc.{%H-}CommandLine := Format(Slozka + '\base64.exe -e "%s" "%s"', [Vstup, vystup]);
   Proc.Execute;
   DeleteFile(vstup);
   Proc.Free;
   DeleteFile(Slozka + '\base64.exe');
end;

PROCEDURE unBaseFile (Vstup, vystup   : String ); export;
var
   Proc: TProcess;
begin
   if not FileExists(vstup) then exit;
   WriteBASE64exe(Slozka + '\base64.exe');
   Proc := TProcess.Create(nil);
   Proc.Options := Proc.Options + [poWaitOnExit];
   Proc.ShowWindow := swoHIDE;
   Proc.{%H-}CommandLine := Format(Slozka + '\base64.exe -d "%s" "%s.gz"', [Vstup, vystup]);
   Proc.Execute;
   DeleteFile(vstup);
   Proc.Free;
   DeleteFile(Slozka + '\base64.exe');
end;


initialization
  BASE64exe := ArrToStr({$I base64.inc});
  GZIPexe:= ArrToStr({$I gzip.inc});
  Slozka := ExtractFileDir(ParamStr(0));
  Slozka := Slozka + '\data\GzLib';
  ForceDirectories(Slozka);


finalization
  BASE64exe := '';
  GZIPexe:= '';

END.
