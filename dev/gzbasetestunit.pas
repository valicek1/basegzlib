UNIT GzBaseTestUnit;

{$mode objfpc}{$H+}

INTERFACE

USES
   Classes,
   Controls,
   Dialogs,
   FileUtil,
   Forms,
   Graphics,
   GzBaseLib,
   StdCtrls,
   SysUtils;

TYPE

   { TtestForm }

   TtestForm = CLASS(TForm )
      btnZakodujEdit  : TButton;
      btnRozkodujText  : TButton;
      btnUnGzBaseFile  : TButton;
      btnBase64  :   TButton;
      btnUnBase64  : TButton;
      btnGzBaseFile  :     TButton;
      Button1: TButton;
      edGzOut  :     TEdit;
      edTestText  :  TEdit;
      edZpracovat  : TEdit;
      lblGzip  :     TLabel;
      lblVstup  :    TLabel;
      lblDelka  :    TLabel;
      PROCEDURE btnBase64Click (Sender   : TObject );
      PROCEDURE btnUnBase64Click (Sender   : TObject );
      PROCEDURE btnUnGzBaseFileClick (Sender   : TObject );
      PROCEDURE btnZakodujEditClick (Sender   : TObject );
      PROCEDURE btnRozkodujTextClick (Sender   : TObject );
      PROCEDURE btnGzBaseFileClick (Sender   : TObject );
      procedure Button1Click(Sender: TObject);
      PROCEDURE edTestTextChange (Sender   : TObject );
   private
      { private declarations }
   public
      { public declarations }
   END;


VAR
   testForm  : TtestForm;

IMPLEMENTATION

{ TtestForm }

PROCEDURE TtestForm.btnZakodujEditClick (Sender   : TObject );
BEGIN
   edTestText.Text := GzBaseStr (edTestText.Text );
END;


PROCEDURE TtestForm.btnUnGzBaseFileClick (Sender   : TObject );
BEGIN
   unGzBaseFile (edGzOut.Text, edZpracovat.Text );
END;


PROCEDURE TtestForm.btnBase64Click (Sender   : TObject );
BEGIN
   edTestText.Text := base64_encode (edTestText.Text );

END;


PROCEDURE TtestForm.btnUnBase64Click (Sender   : TObject );
BEGIN
   edTestText.Text := base64_decode (edTestText.Text );
END;


PROCEDURE TtestForm.btnRozkodujTextClick (Sender   : TObject );
BEGIN
   edTestText.Text := unGzBaseStr (edTestText.Text );

END;


PROCEDURE TtestForm.btnGzBaseFileClick (Sender   : TObject );
BEGIN
   GzBaseFile (edZpracovat.Text, edGzOut.Text );
END;

procedure TtestForm.Button1Click(Sender: TObject);
begin
   StringToFile(base64_decode(FileToString(edZpracovat.Text)), edGzOut.Text);
end;


PROCEDURE TtestForm.edTestTextChange (Sender   : TObject );
BEGIN
   lblDelka.Caption := Format ('Délka řetězce je: %d znaků', [Length (edTestText.Text )] );
END;


{$R *.lfm}

END.
